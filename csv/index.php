<?php
use Phppot\DataSource;

require_once 'DataSource.php';
$db = new DataSource();
$conn = $db->getConnection();

if (isset($_POST["import"])) {
    
    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        $i=0;
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) 
        {
            //mode 1 column
            $parsed_data = explode(",",$column[0]);
            //when there is header
            if(++$i > 1)
            {
                $j=-1;
                $Date = "";
                if (isset($parsed_data[++$j])) {
                    $Date = mysqli_real_escape_string($conn, date('Y-m-d',strtotime($parsed_data[$j])));
                }
                $Hour = "";
                if (isset($parsed_data[++$j])) {
                    $Hour = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $NE = "";
                if (isset($parsed_data[++$j])) {
                    $NE = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Cluster = "";
                if (isset($parsed_data[++$j])) {
                    $Cluster = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter1 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter1 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter2 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter2 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter3 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter3 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter4 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter4 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter5 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter5 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter6 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter6 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                $Counter7 = "";
                if (isset($parsed_data[++$j])) {
                    $Counter7 = mysqli_real_escape_string($conn, $parsed_data[$j]);
                }
                
                $sqlInsert = "INSERT into `raw` (`Date`,`Hour`,`NE`,`Cluster`,`Counter1`,`Counter2`,`Counter3`,`Counter4`,`Counter5`,`Counter6`,`Counter7`)
                    values (?,?,?,?,?,?,?,?,?,?,?)";
                $paramType = "sissiiiiiii";
                $paramArray = array(
                    $Date,
                    $Hour,
                    $NE,
                    $Cluster,
                    $Counter1,
                    $Counter2,
                    $Counter3,
                    $Counter4,
                    $Counter5,
                    $Counter6,
                    $Counter7
                );
                // var_dump($sqlInsert, $paramType, $paramArray);
                // var_dump($paramArray);
                $insertId = $db->insert($sqlInsert, $paramType, $paramArray);
                
                if (! empty($insertId)) {
                    $type = "success";
                    $message = "CSV Data Imported into the Database";
                } else {
                    $type = "error";
                    $message = "Problem in Importing CSV Data";
                }
            }
            
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
<script src="jquery-3.2.1.min.js"></script>

<style>
body {
    font-family: Arial;
    width: 100%;
}

.outer-scontainer {
    background: #F0F0F0;
    border: #e0dfdf 1px solid;
    padding: 20px;
    border-radius: 2px;
}

.input-row {
    margin-top: 0px;
    margin-bottom: 20px;
}

.btn-submit {
    background: #333;
    border: #1d1d1d 1px solid;
    color: #f0f0f0;
    font-size: 0.9em;
    width: 100px;
    border-radius: 2px;
    cursor: pointer;
}

.outer-scontainer table {
    border-collapse: collapse;
    width: 100%;
}

.outer-scontainer th {
    border: 1px solid #dddddd;
    padding: 8px;
    text-align: left;
}

.outer-scontainer td {
    border: 1px solid #dddddd;
    padding: 8px;
    text-align: left;
}

#response {
    padding: 10px;
    margin-bottom: 10px;
    border-radius: 2px;
    display: none;
}

.success {
    background: #c7efd9;
    border: #bbe2cd 1px solid;
}

.error {
    background: #fbcfcf;
    border: #f3c6c7 1px solid;
}

div#response.display-block {
    display: block;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#frmCSVImport").on("submit", function () {

	    $("#response").attr("class", "");
        $("#response").html("");
        var fileType = ".csv";
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
        if (!regex.test($("#file").val().toLowerCase())) {
        	    $("#response").addClass("error");
        	    $("#response").addClass("display-block");
            $("#response").html("Invalid File. Upload : <b>" + fileType + "</b> Files.");
            return false;
        }
        return true;
    });
});
</script>
</head>

<body>
    <!-- <h2>CSV Importer</h2> -->

    <div id="response"
        class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
        <?php if(!empty($message)) { echo $message; } ?>
        </div>
    <div class="outer-scontainer">
        <div class="row">

            <form class="form-horizontal" action="" method="post"
                name="frmCSVImport" id="frmCSVImport"
                enctype="multipart/form-data">
                <div class="input-row">
                    <label class="col-md-4 control-label">Choose CSV
                        File</label> <input type="file" name="file"
                        id="file" accept=".csv">
                    <button type="submit" id="submit" name="import"
                        class="btn-submit">Import</button>
                    <br />

                </div>

            </form>

        </div>
               <?php
            $sqlSelect = "SELECT * FROM `raw`";
            $result = $db->select($sqlSelect);
            if (! empty($result)) {
                ?>
            <table id='rawTable'>
            <thead>
                <tr>
                    <th>RAW ID</th>
                    <th>Date</th>
                    <th>Hour</th>
                    <th>NE</th>
                    <th>Cluster</th>
                    <th>Counter1</th>
                    <th>Counter2</th>
                    <th>Counter3</th>
                    <th>Counter4</th>
                    <th>Counter5</th>
                    <th>Counter6</th>
                    <th>Counter7</th>

                </tr>
            </thead>
<?php
                
                foreach ($result as $row) {
                    ?>
                    
                <tbody>
                <tr>
                    <td><?php  echo $row['raw_id']; ?></td>
                    <td><?php  echo $row['Date']; ?></td>
                    <td><?php  echo $row['Hour']; ?></td>
                    <td><?php  echo $row['NE']; ?></td>
                    <td><?php  echo $row['Cluster']; ?></td>
                    <td><?php  echo $row['Counter1']; ?></td>
                    <td><?php  echo $row['Counter2']; ?></td>
                    <td><?php  echo $row['Counter3']; ?></td>
                    <td><?php  echo $row['Counter4']; ?></td>
                    <td><?php  echo $row['Counter5']; ?></td>
                    <td><?php  echo $row['Counter6']; ?></td>
                    <td><?php  echo $row['Counter7']; ?></td>
                </tr>
                    <?php
                }
                ?>
                </tbody>
        </table>
        <?php } ?>
    </div>

</body>

</html>
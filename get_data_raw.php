<?php
 
 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);
 session_start();
 $tipe = isset($_SESSION['t']) ? $_SESSION['t'] : "";
 $id_user = isset($_SESSION['i']) ? $_SESSION['i'] : "";
 /*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
include_once ("config/functions.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];

$txt_field= "
Date
,Hour
,NE
,Cluster
,Counter1
,Counter2
,Counter3
,Counter4
,Counter5
,Counter6
,Counter7
";
// if(!check_role($filename,''))
// {
//   echo json_encode( array("status" => false,"info" => "You are not authorized.!!!","messages" => "You are not authorized.!!!" ) );
// }
// else
{
    
    $id = isset($_GET['id']) ? $_GET['id'] : ""; 
    $mode = isset($_GET['mode']) ? $_GET['mode'] : ""; 
    // DB table to use
    $table = 'raw';



    // Table's primary key
    $primaryKey = 'raw_id';
    
    // Array of database columns which should be read and sent back to DataTables.
    // The `db` parameter represents the column name in the database, while the `dt`
    // parameter represents the DataTables column identifier. In this case simple
    // indexes
    /*
    $txt_field= "raw_nama,raw_email,raw_tarikh,raw_tempat,raw_agency,program_name_ms,raw_bil_peserta,raw_photo,raw_checkbox1,raw_checkbox2,raw_checkbox3,raw_checkbox4,raw_checkbox5,raw_checkbox6,raw_checkbox7,raw_ulasan";

    */
    // $_COOKIE[$cookie_name]=0;
    // $cookie_name = "oshe";
    // $cookie_value = 0;
    // setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    $i=-1;
    $counter = 0;
    $columns = array(
            array(
                'db'        => 'raw_id',
                'dt'        => ++$i,
                'formatter' => function( $d, $row ) {
                    global $counter;
                        return ++$counter;
                }
            ),
        // ,array(
        //     'db'        => 'raw_id',
        //     'dt'        => ++$i,
        //     'formatter' => function( $d, $row ) {
        //         $tipe = isset($_SESSION['t']) ? $_SESSION['t'] : "";
        //         $mode = isset($_GET['mode']) ? $_GET['mode'] : ""; 

        //         switch($mode)
        //         {
        //             case "draft" : {
        //                 return '<a href="index.php?page=draftoshe&raw_id='.$d.'" class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> | <a onclick="actiondelete(\'oshe\',\'draft\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;

        //             }break;
        //             case "submitted" : {
        //                 if($tipe == "ADMIN")
        //                 {
        //                     return '<a href="index.php?page=submittedoshe&raw_id='.$d.'" class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> | <a onclick="actiondelete(\'oshe\',\'submitted\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;
        //                 }
        //                 else
        //                 {
        //                     return '<a href="index.php?page=submittedoshe&raw_id='.$d.'" class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> ';
        //                     //| <a onclick="actiondelete(\'oshe\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;    
        //                 }
        //             }break;
        //             default : {$pagename = "draftoshe";}break;
                    
        //         }
        //         // if($d)
        //         // {//<img class="direct-chat-img" src="../uploads/oshe/user3-128x128.jpg" alt="Message User Image">
        //         //     // return '|<span><i class="fa fa-delete"></i>view</span>';
        //         //     return '<a href="index.php?page='.$pagename.'&raw_id='.$d.'" class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> | <a onclick="actiondelete(\'oshe\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;
        //         // }
        //         // else
        //         // {
        //         //     return '<a href="index.php?page='.$pagename.'&raw_id='.$d.'" class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> | <a onclick="actiondelete(\'oshe\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;
                    
        //         //     //return '<a href="index.php?page='.$pagename.'&raw_id='.$d.'" class="btn btn-primary">View Detail</a>' ;

        //         //     // return '<a href="index.php?page=draftoshe&raw_id=1'.$d.'">View Detail</a>';
        //         //     // return '<img src="uploads/oshe/avatar5.png" class="direct-chat-img" alt="User Image">';
        //         // }
        //     }
        // ),
        // array( 'db' => 'raw_id',  'dt' => ++$i ),

        array( 'db' => 'Date',   'dt' => ++$i 
                ,'formatter' => function( $d, $row ) {
                    if($d)
                    {
                        $txt_field['Date'] = (new \DateTime($d))->format('d/m/Y');
                        return $txt_field['Date'];
                    }
                    else
                    {
                        return NULL;
                    }
                }
            )        
        ,array( 'db' => 'Hour',  'dt' => ++$i )  
        ,array( 'db' => 'NE',   'dt' => ++$i )  
        ,array( 'db' => 'Cluster',   'dt' => ++$i )  
        ,array( 'db' => 'Counter1',   'dt' => ++$i )  
        ,array( 'db' => 'Counter2',   'dt' => ++$i ) 
        ,array( 'db' => 'Counter3',   'dt' => ++$i )
        ,array( 'db' => 'Counter4',   'dt' => ++$i )
        ,array( 'db' => 'Counter5',   'dt' => ++$i )
        ,array( 'db' => 'Counter6',   'dt' => ++$i )
        ,array( 'db' => 'Counter7',   'dt' => ++$i )
        ,array( 'db' => 'raw_id',   'dt' => ++$i  
            ,'formatter' => function( $d, $row ) {
                //Avail 
                if($row["Counter2"]!=0)
                {
                    $avail = round(($row["Counter1"] / $row["Counter2"]),2)*100;
                }
                else
                {
                    $avail = 0;
                }
                $class = "";
                if($avail>90 && $avail<=100)
                {
                    $class = "text-success";  
                }
                elseif($avail>70 && $avail<=90)
                {
                    $class = "text-warning";  
                }
                elseif($avail>0 && $avail<=70)
                {
                    $class = "text-danger";  
                }
                else
                {
                    $class = "text-info";  
                }
                return '<div><i class="far fa-circle '.$class.'"></i> '.$avail  ." %</div>";
            }
        ) 
        ,array( 'db' => 'raw_id',   'dt' => ++$i  
            ,'formatter' => function( $d, $row ) {
                //RSSI 
                $rssi = ( $row["Counter3"] );
                // return $rssi ." (dBm)";
                $class = "";
                if($rssi>-90 && $rssi<=-80)
                {
                    $class = "text-success";  
                }
                elseif($rssi>-100 && $rssi<=-90)
                {
                    $class = "text-warning";  
                }
                elseif($rssi>-150 && $rssi<=-100)
                {
                    $class = "text-danger";  
                }
                else
                {
                    $class = "text-info";  
                }
                return '<div><i class="far fa-circle '.$class.'"></i> '.$rssi  ." dBm</div>";
            }
        ) 
        ,array( 'db' => 'raw_id',   'dt' => ++$i  
            ,'formatter' => function( $d, $row ) {
                //Drop 
                if($row["Counter5"]!=0)
                {
                    $drop = round(($row["Counter4"] / $row["Counter5"]),2)*100;
                }
                else
                {
                    $drop = 0;
                }
                
                $class = "";
                if($drop>0 && $drop<=5)
                {
                    $class = "text-success";  
                }
                elseif($drop>5 && $drop<=10)
                {
                    $class = "text-warning";  
                }
                elseif($drop>10 && $drop<=100)
                {
                    $class = "text-danger";  
                }
                else
                {
                    $class = "text-info";  
                }
                return '<div><i class="far fa-circle '.$class.'"></i> '.$drop  ." %</div>";
                }
        ) 
        ,array( 'db' => 'raw_id',   'dt' => ++$i  
            ,'formatter' => function( $d, $row ) {
                //Success  
               
                if($row["Counter7"]!=0)
                {
                    $success = round(($row["Counter6"] / $row["Counter7"]),2)*100;
                }
                else
                {
                    $success = 0;
                }
                $class = "";
                if($success>90 && $success<=100)
                {
                    $class = "text-success";  
                }
                elseif($success>70 && $success<=90)
                {
                    $class = "text-warning";  
                }
                elseif($success>0 && $success<=70)
                {
                    $class = "text-danger";  
                }
                else
                {
                    $class = "text-info";  
                }
                return '<div><i class="far fa-circle '.$class.'"></i> '.$success  ." %</div>";
                }
        ) 

    );
    

    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * If you just want to use the basic configuration for DataTables with PHP
    * server-side, there is no need to edit below this line.
    */
    
    require( 'ssp.class.php' );
    
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    );
    // $created_by = " raw_created_by = ".$id_user;

    // if($tipe == "ADMIN")
    // {
    //     $created_by = " raw_is_deleted = 0 ";
    // }
    // if($id=="")
    // {
    //     echo json_encode(
    //         SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    //     );
    // }
    // else
    // {
    //     if($mode=="draft")
    //     {
    //         $extra ='GROUP BY id DESC';
    //         echo json_encode(
    //             SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,null, $created_by ." AND raw_status = 2 " )
    //         );
    //     }
    //     else
    //     {
    //         echo json_encode(
    //             SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,null, $created_by ." AND raw_status = 1 " )
    //         );
    //     }
        
    // }

}       
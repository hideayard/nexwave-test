
<?php
// if(!check_role($page,'*'))
// {
//   echo "<script>alert('You are not permitted!!!');window.location='home';</script>";
// }

$sql = "SELECT Date, NE, Hour, (Counter1/Counter2)*100 as avail, (Counter4/Counter5) as drops, Counter3 as rssi, (Counter6/Counter7)*100 as success  
FROM `raw` 
GROUP BY Date, NE, Hour
ORDER BY Date ASC, NE, Hour ASC" ;
$sql_NE = "select distinct NE from raw;";
$sql_Hour = "select distinct Hour from raw;";
$result = $db->rawQuery($sql);
$result_NE = $db->rawQuery($sql_NE);
$result_Hour = $db->rawQuery($sql_Hour);
foreach ($result_Hour as $key => $value) 
{
    $arrHour[] = $value['Hour'];
}
$color = ["#FF5733","#DBFF33","#33FFBD","#FFBD33","#DBFF33","#33FF57"];
foreach ($result_NE as $key => $value) 
{
  // $color[] = rand_color();
    $arrNE[] = $value['NE'];
    $nResults[$value['NE']] = filter_by_value($result, 'NE', $value['NE']);
    // echo "<hr>";
    foreach ($nResults[$value['NE']] as $key2 => $value2) 
    {
      // var_dump($value2);
      $arrResult['avail'][$value['NE']][] = $value2['avail'];
      $arrResult['success'][$value['NE']][] = $value2['success'];
      $arrResult['rssi'][$value['NE']][] = $value2['rssi'];
      $arrResult['drops'][$value['NE']][] = $value2['drops'];
      
    }
}
// var_dump($arrResult['NE1']);
// var_dump($nResults['NE1']);
// $unik = array_unique($result);
// var_dump($nResults['NE1']);
//  echo implode(",",$arrResult['NE1']);
 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Statistic Overview</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Statistic</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                
                
                <div class="col-md-6">
                  <div class="card card-info">
                      <div class="card-header">
                        <h4 class="card-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                          AVAIL Report
                          </a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse show ">
                        <div class="card-body">
                              <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                  <canvas id="avail-report" height="400" style="display: block; height: 200px; width: 311px;" width="622" class="chartjs-render-monitor"></canvas>
                              </div>

                              <div class="d-flex flex-row justify-content-end">
                                  <span class="mr-2">
                                    <?php
                                    $i=0;
                                     foreach ($result_NE as $key => $value) 
                                     {
                                      echo ' <i class="fas fa-square " style="color:'.$color[$i].';"></i>'.$value['NE'];$i++;
                                     }
                                    ?>
                                   
                                  </span>
                              </div>
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="col-md-6">

                    <div class="card card-success">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                            SUCCESS Report
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse  show ">
                          <div class="card-body">
                            
                                <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <canvas id="success-report" height="400" style="display: block; height: 200px; width: 311px;" width="622" class="chartjs-render-monitor"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                    <span class="mr-2">
                                        <?php
                                        $i=0;
                                        foreach ($result_NE as $key => $value) 
                                        {
                                          echo ' <i class="fas fa-square " style="color:'.$color[$i].';"></i>'.$value['NE'];$i++;
                                        }
                                        ?>
                                    </span>
                                </div>
                            
                          </div>
                        </div>
                      </div>

                </div>

                <div class="col-md-6">

                    <div class="card card-warning">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                              RSSI Report
                            </a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse show ">
                          <div class="card-body">
                            
                                <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <canvas id="rssi-report" height="400" style="display: block; height: 200px; width: 311px;" width="622" class="chartjs-render-monitor"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                    <span class="mr-2">
                                      <?php
                                          $i=0;
                                          foreach ($result_NE as $key => $value) 
                                          {
                                            echo ' <i class="fas fa-square " style="color:'.$color[$i].';"></i>'.$value['NE'];$i++;
                                          }
                                          ?>
                                    </span>
                                </div>

                          </div>
                        </div>
                      </div>

                </div>

                <div class="col-md-6">

                    <div class="card card-danger">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                              DROP Report
                            </a>
                          </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse show ">
                          <div class="card-body">
                            
                                <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <canvas id="drops-report" height="400" style="display: block; height: 200px; width: 311px;" width="622" class="chartjs-render-monitor"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                      <span class="mr-2">
                                          <?php
                                              $i=0;
                                              foreach ($result_NE as $key => $value) 
                                              {
                                                echo ' <i class="fas fa-square " style="color:'.$color[$i].';"></i>'.$value['NE'];$i++;
                                              }
                                          ?>                                    
                                        </span>
                                </div>

                          </div>
                        </div>
                      </div>

                </div>

              
            </div>

            <!-- /.row -->
            <div class="row col-lg-12" style="text-align:center;">
                <div class="col-lg-3 col-6">
                    <a href="home"><button type="button" class="btn btn-block btn-primary">Back</button></a>
                </div>
                <!-- ./col -->
            </div>


        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>


<script>
$(function () {
  'use strict'

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true

  // var $osheChart = $('#avail-report')
  // var osheChart  = new Chart($osheChart, {
  //   type   : 'bar',
  //   data   : {
  //     labels  : [implode(",",$result_Hour);],
  //     datasets: [
  //       {
  //         backgroundColor: '#007bff',
  //         borderColor    : '#007bff',
  //         data           : [
  //           <?php
  //           $nResults = filter_by_value($result, 'NE', '2');

  //                                         foreach ($result as $key => $value) 
  //                                         {
  //                                           echo '<i class="fas fa-square text-primary"></i>'.$value['NE'];
  //                                         }
  //                                         ?>
  //           ]
  //       }
        
  //     ]
  //   },
  //   options: {
  //     maintainAspectRatio: false,
  //     tooltips           : {
  //       mode     : mode,
  //       intersect: intersect
  //     },
  //     hover              : {
  //       mode     : mode,
  //       intersect: intersect
  //     },
  //     legend             : {
  //       display: false
  //     },
  //     scales             : {
  //       yAxes: [{
  //         // display: false,
  //         stacked:true,
  //         gridLines: {
  //           display      : true,
  //           lineWidth    : '4px',
  //           color        : 'rgba(0, 0, 0, .6)',
  //           zeroLineColor: 'transparent'
  //         }
  //         // ,ticks    : $.extend({
  //         //   beginAtZero: true,

  //         //   // Include a dollar sign in the ticks
  //         //   callback: function (value, index, values) {
  //         //     if (value >= 1000) {
  //         //       value /= 1000
  //         //       value += 'k'
  //         //     }
  //         //     // return 'MYR' + value
  //         //     return  value
  //         //   }
  //         // }, ticksStyle)
  //       }],
  //       xAxes: [{
  //         stacked:true,
  //         display  : true,
  //         gridLines: {
  //           display: false
  //         }
  //         // ,ticks    : ticksStyle
  //       }]
  //     }
  //   }
  // });

  var $availChart = $('#avail-report');
  var availChart  = new Chart($availChart, {
    // type   : 'line',
    data   : {
      labels  : [<?php echo implode(",",$arrHour);?>],
      datasets: [
        <?php
        $i=0;
          foreach ($result_NE as $key => $value) 
          {
        ?>
        {
          type                : 'line',
          data           : [<?=implode(",",$arrResult['avail'][$value['NE']])?>],
          backgroundColor     : 'transparent',
          borderColor         : '<?=$color[$i]?>',
          pointBorderColor    : '<?=$color[$i]?>',
          pointBackgroundColor: '<?=$color[$i]?>',
          fill                : false
        },
      <?php $i++;} ?>
       
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            // //suggestedMax: 150
          }, ticksStyle)
        }],
        xAxes: [{
          scaleLabel: {
              display: true,
              labelString: 'Hour'
            },
          display  : true,
          gridLines: {
            display: true
          },
          ticks    : ticksStyle
        }]
      }
    }
  });


  var $successChart = $('#success-report');
  var successChart  = new Chart($successChart, {
    // type   : 'line',
    data   : {
      labels  : [<?php echo implode(",",$arrHour);?>],
      datasets: [
        <?php
        $i=0;
          foreach ($result_NE as $key => $value) 
          {
        ?>
        
        {
          type                : 'line',
          data           : [<?=implode(",",$arrResult['success'][$value['NE']])?>],
          backgroundColor     : 'transparent',
          borderColor         : '<?=$color[$i]?>',
          pointBorderColor    : '<?=$color[$i]?>',
          pointBackgroundColor: '<?=$color[$i]?>',
          fill                : false
        },
      <?php $i++;} ?>
       
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            //suggestedMax: 150
          }, ticksStyle)
        }],
        xAxes: [{
          scaleLabel: {
              display: true,
              labelString: 'Hour'
            },
          display  : true,
          gridLines: {
            display: true
          },
          ticks    : ticksStyle
        }]
      }
    }
  });

  var $rssiChart = $('#rssi-report');
  var rssiChart  = new Chart($rssiChart, {
    // type   : 'line',
    data   : {
      labels  : [<?php echo implode(",",$arrHour);?>],
      datasets: [
        <?php
        $i=0;
          foreach ($result_NE as $key => $value) 
          {
        ?>
        
        {
          type                : 'line',
          data           : [<?=implode(",",$arrResult['rssi'][$value['NE']])?>],
          backgroundColor     : 'transparent',
          borderColor         : '<?=$color[$i]?>',
          pointBorderColor    : '<?=$color[$i]?>',
          pointBackgroundColor: '<?=$color[$i]?>',
          fill                : false
        },
      <?php $i++;} ?>
       
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            //suggestedMax: 150
          }, ticksStyle)
        }],
        xAxes: [{
          scaleLabel: {
              display: true,
              labelString: 'Hour'
            },
          display  : true,
          gridLines: {
            display: true
          },
          ticks    : ticksStyle
        }]
      }
    }
  });

  var $dropsChart = $('#drops-report');
  var dropsChart  = new Chart($dropsChart, {
    // type   : 'line',
    data   : {
      labels  : [<?php echo implode(",",$arrHour);?>],
      datasets: [
        <?php
        $i=0;
          foreach ($result_NE as $key => $value) 
          {
        ?>
        
        {
          type                : 'line',
          data           : [<?=implode(",",$arrResult['drops'][$value['NE']])?>],
          backgroundColor     : 'transparent',
          borderColor         : '<?=$color[$i]?>',
          pointBorderColor    : '<?=$color[$i]?>',
          pointBackgroundColor: '<?=$color[$i]?>',
          fill                : false
        },
      <?php $i++;} ?>
       
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            // //suggestedMax: 150
          }, ticksStyle)
        }],
        xAxes: [{
          scaleLabel: {
              display: true,
              labelString: 'Hour'
            },
          display  : true,
          gridLines: {
            display: true
          },
          ticks    : ticksStyle
        }]
      }
    }
  });
  



});

</script>

<?php
function filter_by_value ($array, $index, $value){
    $newarray = null;
    if(is_array($array) && count($array)>0) 
    {
        foreach(array_keys($array) as $key){
            $temp[$key] = $array[$key][$index];
            
            if ($temp[$key] == $value){
                $newarray[$key] = $array[$key];
            }
        }
      }
    return $newarray;
  
  }

  function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}
?>
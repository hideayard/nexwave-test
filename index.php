<?php
session_start();
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$prefix = "/nw";

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
// include("config/functions.php");

date_default_timezone_set("Asia/Jakarta");
$tgl=date('Y-m-d');
$page=isset($_GET['page']) ? $_GET['page'] : "home"; 
$mode=isset($_GET['mode']) ? $_GET['mode'] : ""; 
$d=isset($_GET['d']) ? $_GET['d'] : ""; 
// echo $page;die;
if(($page == 'temp')||($page == 'temp.php'))
{
  header('Location:temp.php');
  exit(); //hentikan eksekusi kode di login_proses.php
}
if(!isset($_SESSION['u']))
{
  header('Location:login.php');
  exit(); //hentikan eksekusi kode di login_proses.php
}

$sql = "SELECT * FROM users WHERE user_id = '".$_SESSION['i']."' "; 
$result = $db->rawQuery($sql);//@mysql_query($sql);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nexwave Technology | Dashboard</title>
  <link rel="icon" href="assets/img/al.png" type="image/png" sizes="50x50">  

  <meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Ionicons -->
 <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
  <!-- Theme style dist/js/bootstrap-datepicker.min.js -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- <script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script> -->
  <!-- <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"> -->

  <!-- <link rel="stylesheet" href="css/main_loader.css"> -->
  <!-- <script src="js/modernizr-2.6.2.min.js"></script> -->
  
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/dropzone/min/dropzone.min.js"></script> 
<link href="plugins/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css">
<link href="plugins/dropzone/min/basic.min.css" rel="stylesheet" type="text/css">
<style>
        .dropzone-previews {
            /* height: 100px; */
            width: 100%;
            border: dashed 1px blue;
            background-color: lightblue;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="home" class="nav-link">Home</a>
      </li>
      
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
 
    </ul>
  </nav>
  <!-- /.navbar -->

<?php  
require_once ("sidebar.php"); 

  $users = $db->get('users', 10); //contains an Array 10 users
  //print_r($users);
  if (file_exists("".$page.".php")) 
  {
      include("".$page.".php");
  }
  else 
  {
      include("error.php");
  }
?>

  
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 <a href="#">Mac Kayver Studio</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<!-- <script src="plugins/toastr/toastr.min.js"></script> -->
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<style>
  table.dataTable tbody th,
table.dataTable tbody td {
    white-space: nowrap;
}
</style>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.1.2/js/dataTables.rowGroup.min.js"></script> 
<!-- AdminLTE App -->
<script src="dist/js/dataTables.buttons.min.js"></script>
<!-- <script src="dist/js/buttons.flash.min.js"></script> -->
<!-- <script src="dist/js/pdfmake.min.js"></script> -->
<!-- <script src="dist/js/vfs_fonts.js"></script> -->
<!-- <script src="dist/js/buttons.html5.min .js"></script> -->
<!-- <script src="dist/js/buttons.print.min.js"></script> -->
<!-- <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script> -->



<script>

function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
          return true;
      }

$(document).ready(function() {
	
	setTimeout(function(){
		$('body').addClass('loaded');
		// $('h1').css('color','#222222');
	}, 1000);
	
});

  // (function ($) {
  // })(jQuery);
  $(function () {
    var tabelraw = $('#tabelraw').DataTable({ //});
      "orderCellsTop": true,
      "fixedHeader": true,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      // "processing": true,
      // "serverSide": true,
      "scrollX": true,
      "scrollY": "500px",
      // "scrollCollapse": true,
      // "dom": 'Bfrtip',
      rowGroup: {
            startRender: null,
            endRender: function ( rows, group ) {
              // var sumHour = rows
              //       .data()
              //       .pluck(1)
              //       .reduce( function (a, b) {
              //           return a + b.replace(/[^\d]/g, '')*1;
              //       }, 0) / rows.count();
              //       sumHour = $.fn.dataTable.render.number(',', '.', 0, 'H').display( sumHour );
              //   console.log('group',group)
              //   console.log('sumHour',sumHour)
              let sumHour = rows
                    .data()
                    .pluck(1)
                    .reduce( function (a, b) {
                        return a + b*1;
                    }, 0) ;/// rows.count();
          let i = 2;
          let sumCounter = new Array();
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );/// rows.count() );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
              sumCounter.push( rows.data().pluck(++i).reduce( function (a, b) { return parseInt(a) + parseInt(b); }, 0) );
                    

                let ic = -1;
                    return $('<tr class="bg-info"/>')
                    .append( '<td colspan="1">'+group+' ('+rows.count()+')</td>' )
                    // .append( '<td>'+ageAvg.toFixed(0)+'</td>' )
                    // .append( '<td/>' )
                    .append( '<td colspan="1">('+sumHour+')</td>' )
                    .append( '<td>-</td>' )
                    // .append( '<td>('+sumHour+')</td>' )
                    .append( '<td>sum('+sumCounter[++ic]+')</td>' ) //counter 1
                    .append( '<td>sum('+sumCounter[++ic]+')</td>' ) //counter 2
                    .append( '<td>avg('+ parseInt(sumCounter[++ic] / rows.count() ).toFixed(2)+')</td>' ) //counter 3
                    .append( '<td>sum('+sumCounter[++ic].toFixed(2)+')</td>' ) //counter 4
                    .append( '<td>sum('+sumCounter[++ic].toFixed(2)+')</td>' ) //counter 5
                    .append( '<td>sum('+sumCounter[++ic].toFixed(2)+')</td>' ) //counter 6
                    .append( '<td>sum('+sumCounter[++ic].toFixed(2)+')</td>' ) //counter 7
                    .append( '<td>avg('+ parseFloat(sumCounter[++ic] / rows.count() ).toFixed(2)+')</td>' ) //counter 3
                    .append( '<td>avg('+ parseFloat(sumCounter[++ic] / rows.count() ).toFixed(2)+')</td>' ) //counter 3
                    // .append( '<td>avg('+sumCounter[++ic]+')</td>' ) // rssi
                    .append( '<td>avg('+ parseFloat(sumCounter[++ic] / rows.count() ).toFixed(2)+')</td>' ) //counter 3
                    .append( '<td>avg('+ parseFloat(sumCounter[++ic] / rows.count() ).toFixed(2)+')</td>' ) //counter 3
                // return "<tr><td>"+group +' ('+rows.count()+')"</td>"'+"<td colspan='9'>"+sumHour+"</td></tr>";
            },
            dataSrc: 0
        }
      // rowGroup: {
      //       startRender: null,
      //       endRender: function ( rows, group ) {
      //           var salaryAvg = rows
      //               .data()
      //               .pluck(5)
      //               .reduce( function (a, b) {
      //                   return a + b.replace(/[^\d]/g, '')*1;
      //               }, 0) / rows.count();
      //           salaryAvg = $.fn.dataTable.render.number(',', '.', 0, '$').display( salaryAvg );
      //           console.log('group',group)
      //           console.log('salaryAvg',salaryAvg)
      //           var ageAvg = rows
      //               .data()
      //               .pluck(3)
      //               .reduce( function (a, b) {
      //                   return a + b*1;
      //               }, 0) / rows.count();
      //               console.log('ageAvg',ageAvg)

      //           return $('<tr/>')
      //               .append( '<td colspan="3">Averages for '+group+'</td>' )
      //               .append( '<td>'+ageAvg.toFixed(0)+'</td>' )
      //               .append( '<td/>' )
      //               .append( '<td>'+salaryAvg+'</td>' );
      //       },
      //       dataSrc: 3
      //   },
    }).columns.adjust();
    // $('#example2').DataTable({
      var tabel = $('#example2').DataTable({
      "orderCellsTop": true,
      "fixedHeader": true,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "scrollY": "500px",
      "scrollCollapse": true,
      "dom": 'Bfrtip',
      "buttons": [
        // 'print', 'pdf', 'csv',
          // {text: 'Reload',
          // action: function ( e, dt, node, config ) {
          //     dt.ajax.reload();
          // }
          // }
      ],
      rowGroup: {
            startRender: null,
            endRender: function ( rows, group ) {
                return group +' ('+rows.count()+')';
            },
            dataSrc: 1
        },
      // rowGroup: {
      //       startRender: null,
      //       endRender: function ( rows, group ) {
      //           var salaryAvg = rows
      //               .data()
      //               .pluck(5)
      //               .reduce( function (a, b) {
      //                   return a + b.replace(/[^\d]/g, '')*1;
      //               }, 0) / rows.count();
      //           salaryAvg = $.fn.dataTable.render.number(',', '.', 0, '$').display( salaryAvg );
      //           console.log('group',group)
      //           console.log('salaryAvg',salaryAvg)
      //           var ageAvg = rows
      //               .data()
      //               .pluck(3)
      //               .reduce( function (a, b) {
      //                   return a + b*1;
      //               }, 0) / rows.count();
      //               console.log('ageAvg',ageAvg)

      //           return $('<tr/>')
      //               .append( '<td colspan="3">Averages for '+group+'</td>' )
      //               .append( '<td>'+ageAvg.toFixed(0)+'</td>' )
      //               .append( '<td/>' )
      //               .append( '<td>'+salaryAvg+'</td>' );
      //       },
      //       dataSrc: 3
      //   },
        <?php
        switch($page)
        {
          case "users" : {echo '"ajax": "get_data_users.php"';}break;
          case "home" : {echo '"ajax": "get_data_video.php"';}break;
          case "rawlist" : {
            echo '"ajax": "get_data_raw.php?d='.$d.'","order": [[ 1, "desc" ]]';
          }break;

        }
        ?>
        
    });

  });

  function get_update()
{
  $.ajax({
        url:"http://<?=$_SERVER['HTTP_HOST'].$prefix?>/actionrefresh.php",
        method:"POST", //First change type to method here
        success:function(response) {
        //  alert(response);
        // console.log(response);
        let myObj = JSON.parse(response);
        myObj.forEach(peritem);

        
        // console.log(myObj[0].txt_id_1 +"||" + myObj[0].txt_nama_1)
        // document.getElementById()
        console.log(myObj);
        // document.getElementById('txt_nama_').innerHTML = response.node_nama;
        
        // document.getElementById('jam').innerHTML = response;
       },
       error:function(e){
        alert("error = "+e);
       }

      });
      // let t = setTimeout(get_update, 5000);
}
// get_update();

function actiondelete(module,type,module_id)
{
 
  Swal.fire({
  title: 'Are you sure?',
  // text: "Delete Data ",
  // html: '<p class="text text-danger">'+filename+'</p>',
  // icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    var data = new FormData();
    data.append("mode", "delete");//module);
    data.append("type", type);
    data.append(module+"_id", module_id);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "action"+module+".php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
          var rv;
          try {
            rv = JSON.parse(data);
            if(isEmpty(rv))
            {
                    Swal.fire(
                    'Info!',
                    'No Data!',
                    'info'
                    );
                console.log("NO DATA : ", data);
                $("#btnLoadMore").html('Load More');
            }
            else
            {
              if(rv.info==1)
              {
                Swal.fire(
                    'Success!',
                    'Success Delete Data!',
                    'success'
                    );
                console.log("SUCCESS Delete: ", data);
               setTimeout(function(){ $('#example2').DataTable().ajax.reload(); }, 1000);
              }
              else if(rv.info==2)
              {
                Swal.fire(
                      'Success!',
                      'Success Delete Data!',
                      'success'
                      );
                console.log("SUCCESS DELETE : ", data);
                setTimeout(function(){ $('#example2').DataTable().ajax.reload(); }, 1000);
              }

            }
          } catch (e) {
            //error data not json
            Swal.fire(
                    'error!',
                    'Error Delete Data, '+data,
                    'error'
                    );
                
                console.log("ERROR : ", data);
          } 
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
//  console.log(document.getElementById("filecount").value,document.getElementById("maxfile").value);
 //  $("div#my-awesome-dropzone").dropzone.options.maxFiles = 5;//document.getElementById("maxfile").value ;
    
  }
})
 
}

</script>
<script src="plugins/chart.js/Chart.min.js"></script>


<!-- <script type="text/javascript" src="dist/js/jquery-barcode.js"></script>
    <script type="text/javascript">
    
      function generateBarcode(){
        var value = no_ticket;//document.getElementById('ticket').value;//'0033';//$("#barcodeValue").val();
        var btype = 'code128';//$("input[name=btype]:checked").val();
        var renderer = 'css';//$("input[name=renderer]:checked").val();

        var settings = {
          output:renderer,
          bgColor: '#FFFFFF',//$("#bgColor").val(),
          color: '#000000',//$("#color").val(),
          barWidth: '5',//$("#barWidth").val(),
          barHeight: '100',//$("#barHeight").val(),
          moduleSize: '5',//$("#moduleSize").val(),
          posX: '10',//$("#posX").val(),
          posY: '20',//$("#posY").val(),
          addQuietZone: '1'//$("#quietZoneSize").val()
        };
        if ($("#rectangular").is(':checked') || $("#rectangular").attr('checked')){
          value = {code:value, rect: true};
        }
        if (renderer == 'canvas'){
          clearCanvas();
          $("#barcodeTarget").hide();
          $("#canvasTarget").show().barcode(value, btype, settings);
        } else {
          $("#canvasTarget").hide();
          $("#barcodeTarget").html("").show().barcode(value, btype, settings);
        }
      }
          
      function showConfig1D(){
        $('.config .barcode1D').show();
        $('.config .barcode2D').hide();
      }
      
      function showConfig2D(){
        $('.config .barcode1D').hide();
        $('.config .barcode2D').show();
      }
      
      function clearCanvas(){
        var canvas = $('#canvasTarget').get(0);
        var ctx = canvas.getContext('2d');
        ctx.lineWidth = 1;
        ctx.lineCap = 'butt';
        ctx.fillStyle = '#FFFFFF';
        ctx.strokeStyle  = '#000000'; 
        ctx.clearRect (0, 0, canvas.width, canvas.height);
        ctx.strokeRect (0, 0, canvas.width, canvas.height);
      }
      
      $(function(){
        $('input[name=btype]').click(function(){
          if ($(this).attr('id') == 'datamatrix') showConfig2D(); else showConfig1D();
        });
        $('input[name=renderer]').click(function(){
          if ($(this).attr('id') == 'canvas') $('#miscCanvas').show(); else $('#miscCanvas').hide();
        });
        // generateBarcode();
        //Timepicker
        // $('#timepicker').datetimepicker({
        //   format: 'LT'
        // })
  


      });
  
    </script> -->
</body>
</html>
